# [0.1.1]

## Feature

+ 1、使用Levenshtein距离算法测量两个字符串之间的差异。
+ 2、支持纯英文，纯中文，中英混合字符串

## Bugfix

+ 暂无

## Remove

+ 暂无

<br>
